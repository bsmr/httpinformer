package main

import (
	"flag"

	"code.adestis.net/bmgo/httpinformer"
)

func main() {
	var addr, port, title string
	var silent bool

	flag.StringVar(&addr, "addr", "", "address to listen on")
	flag.StringVar(&port, "port", "8765", "port to listen on")
	flag.StringVar(&title, "title", "ADESTIS HTTP Informer", "title of the HTTP page")
	flag.BoolVar(&silent, "silent", false, "disable console output")

	flag.Parse()

	svc := httpinformer.New(addr, port, title, silent)
	if svc == nil {
		panic("failed to get service instance")
	}
	svc.Service()
}
