# build stage
FROM golang:latest AS build-env
#RUN apk --no-cache add build-base git bzr mercurial gcc
ADD . /project
RUN cd /project && CGO_ENABLED=0 GOOS=linux go build -o bin/httpinformer cmd/httpinformer/main.go

# final stage
FROM alpine:latest
WORKDIR /app/
COPY --from=build-env /project/bin/httpinformer /app/
EXPOSE 8765
CMD ["./httpinformer", "-silent"]

# === End Of File ===
