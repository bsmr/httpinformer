# httpinformer

Dump HTTP Request information.

## Docker

```shell
# build
docker build -t bsmr/httpinformer:0.0.2 .
# push (to hub.docker.com)
docker push bsmr/httpinformer:0.0.2
# run
docker run -d --restart unless-stopped -p 8765:8765 --name httpinformer bsmr/httpinformer:0.0.2
```

<!-- End Of File -->