package httpinformer

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"text/template"
	"time"
)

// Informer holds needed information.
type Informer struct {
	Addr   string
	Port   string
	Title  string
	Code   string
	Silent bool
}

// New returns a new Informer instance.
func New(addr, port, title string, silent bool) *Informer {
	return &Informer{
		Addr:   addr,
		Port:   port,
		Title:  title,
		Silent: silent,
	}
}

const (
	failed              = "*failed*"
	defaultPageTemplate = `<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>{{.Title}}</title>
</head>
<body>
	<h1>{{.Title}}</h1>
	<pre>{{.Code}}</pre>
</body>
</html>`
)

// WriteSprintf combines WriteString and Sprintf.
func WriteSprintf(b *bytes.Buffer, format string, args ...interface{}) {
	b.WriteString(fmt.Sprintf(format, args...))
}

// Service starts the service
func (i *Informer) Service() {
	addrport := fmt.Sprintf("%s:%s", i.Addr, i.Port)

	pageTemplate, err := template.New("default").Parse(defaultPageTemplate)
	if err != nil {
		panic(err)
	}

	handler := func(w http.ResponseWriter, req *http.Request) {
		var codeBuffer bytes.Buffer

		// header
		codeBuffer.WriteString("\n")
		WriteSprintf(&codeBuffer, "Timestamp: %s\n", time.Now().UTC().String())
		codeBuffer.WriteString("\n")

		// request information
		WriteSprintf(&codeBuffer, "=== Request Information ===\n\n")
		WriteSprintf(&codeBuffer, "RemoteAddr: %q\n", req.RemoteAddr)
		WriteSprintf(&codeBuffer, "Method: %q\n", req.Method)
		WriteSprintf(&codeBuffer, "URL: %q\n", req.URL)
		WriteSprintf(&codeBuffer, "RequestURI: %q\n", req.RequestURI)
		WriteSprintf(&codeBuffer, "Proto: %q\n", req.Proto)
		codeBuffer.WriteString("Headers:\n")
		for k, v := range req.Header {
			codeBuffer.WriteString(fmt.Sprintf(" - %q: %q\n", k, v))
		}
		codeBuffer.WriteString("\n")

		// OS information
		WriteSprintf(&codeBuffer, "=== OS Information ===\n\n")
		var hostname string
		if hostname, err = os.Hostname(); err != nil {
			hostname = failed
		}
		WriteSprintf(&codeBuffer, "Hostname: %q\n", hostname)
		var executeable string
		if executeable, err = os.Executable(); err != nil {
			executeable = failed
		}
		WriteSprintf(&codeBuffer, "Executable: %q\n", executeable)
		WriteSprintf(&codeBuffer, "Process ID: %d\n", os.Getpid())
		WriteSprintf(&codeBuffer, "User ID: %d, Group ID: %d\n", os.Getuid(), os.Getgid())
		var userHomeDir string
		if userHomeDir, err = os.UserHomeDir(); err != nil {
			userHomeDir = failed
		}
		WriteSprintf(&codeBuffer, "User Home Directory: %q\n", userHomeDir)
		var workDirectory string
		if workDirectory, err = os.Getwd(); err != nil {
			workDirectory = failed
		}
		WriteSprintf(&codeBuffer, "Work Directory: %q\n", workDirectory)

		WriteSprintf(&codeBuffer, "Environment:\n")
		for _, e := range os.Environ() {
			s := strings.Split(e, "=")
			if len(s) != 2 {
				continue
			}
			k := s[0]
			v := s[1]
			WriteSprintf(&codeBuffer, " - %q: %q\n", k, v)
		}
		codeBuffer.WriteString("\n")

		code := codeBuffer.String()

		data := struct {
			Title, Code string
		}{
			Title: i.Title,
			Code:  code,
		}

		var pageBuffer bytes.Buffer
		pageWriter := io.Writer(&pageBuffer)
		err = pageTemplate.Execute(w, data)
		if err != nil {
			panic(err)
		}
		page := pageBuffer.String()

		io.WriteString(pageWriter, page)
		if !i.Silent {
			fmt.Fprintln(os.Stderr, "----------------------------------------")
			fmt.Fprintln(os.Stderr, code)
		}
	}

	http.HandleFunc("/", handler)

	if !i.Silent {
		fmt.Fprintln(os.Stderr, "Listener starting...")
		defer fmt.Fprintln(os.Stderr, "Listener terminating...")
	}
	if err := http.ListenAndServe(addrport, nil); err != nil {
		panic(err)
	}
}
